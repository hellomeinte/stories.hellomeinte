var express     = require('express'),
    path        = require('path'),
    router      = express.Router(),
    models,
    services

function renderStory(res){
    return function onStory(error,story){
        story.config={
            screen_sizes:{
                xs:process.env.XS_WIDTH,
                sd:process.env.SD_WIDTH,
                md:process.env.MD_WIDTH,
                hd:process.env.HD_WIDTH,
                xl:process.env.XL_WIDTH
            }
        }
        res.render('story', story);
    }
}

router.get('/',function(req,res,next){
    models.stories.getRandomStory(renderStory(res))
})

router.get('/stories/:id', function(req, res, next) {
    var id = req.params.id;
    models.stories.getStoryById(title,renderStory(res))
    
});

router.get('/photography/:story/:quality/:imageName',function(req,res,next){
    var quality=req.params.quality
    var imageDir=path.join(req.params.story,req.params.imageName)
    var maxWidth = parseInt(req.query.maxWidth);
    services.imageQuality.get_imageQuality(imageDir,quality,maxWidth,function(error,output){
        var imagePath = output.split('public')[1]
        var options={
            root: path.join(__dirname,'/../'),
            dotfiles: 'deny',
            headers: {
                'content-type':'image/jpeg'
            }
        }
        res.sendFile(output,options)
    })
    
})

module.exports = function(_models,_services){
    models = _models
    services = _services
    return router
}
