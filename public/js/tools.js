/**
 * ...
 * @author Meinte van't Kruis http://hellomeinte.com/
 */

var T = {};

T.pos=function(elem,x,y,positioning){
	if(!positioning)positioning='relative';

	elem.style.position = positioning;
	elem.style.top = y+'px';
	elem.style.left = x+'px';
}

T.scale=function(elem,value){
	var sString = "scale("+value+","+value+")";

	elem.style.webkitTransform = sString;
	elem.style.MozTransform = sString;
	elem.style.msTransform =sString;
	elem.style.OTransform = sString;
	elem.style.transform = sString;
}

T.del = function(elem) {
	if(!elem)return;
	if(!elem.parentNode)return;
	elem.parentNode.removeChild(elem);

}

T.uniq_arr = function(arr) {
	var u = {}, a = [];
	for (var i = 0, l = arr.length; i < l; ++i) {
		if (u.hasOwnProperty(arr[i])) {
			continue;
		}
		a.push(arr[i]);
		u[arr[i]] = 1;
	}
	return a;
}

T.enbl=function(elem){
	if(!elem.mouseShield)return;
	elem.disabled=false;
	var mouseShield = elem.mouseShield;
	T.del(mouseShield);
	elem.mouseShield=null;
}

T.dsbl=function(elem){
	if(elem.mouseShield)return;
	elem.disabled=true;
	var bRect = elem.getBoundingClientRect();

	var mouseShield = document.createElement('div');

	mouseShield.style.width=bRect.width+'px';
	mouseShield.style.height=bRect.height+'px';
	mouseShield.style.backgroundColor='red';
	mouseShield.style.opacity=0;
	mouseShield.style['top']=bRect.top+'px';
	mouseShield.style['left']=bRect.left+'px';

	T.adcl(mouseShield,'drag_mouseshield');
	document.body.appendChild(mouseShield);

	elem.mouseShield = mouseShield;

}

T.clone = function(parent, elem,insertBefore) {
	if (!elem) throw new Error("Element to clone is null");
	var clone = elem.cloneNode(true);
	var nodes = elem.childNodes;
	T.rmcl(clone, 'template');
    clone.removeAttribute('id');

	if(parent){
        parent.appendChild(clone);
    }
	return clone;
}

T.cIndex=function(element,child){
    return [].indexOf.call(element.children, child); //this is the array data index
}

T.gcls = function(node, classname) {
	if (node.getElementsByClassName) { // use native implementation if available
		return node.getElementsByClassName(classname);
	} else {
		return (function getElementsByClass(searchClass, node) {
			if (node == null)
				node = document;
			var classElements = [],
				els = node.getElementsByTagName("*"),
				elsLen = els.length,
				pattern = new RegExp("(^|\\s)" + searchClass + "(\\s|$)"),
				i, j;

			for (i = 0, j = 0; i < elsLen; i++) {
				if (pattern.test(els[i].className)) {
					classElements[j] = els[i];
					j++;
				}
			}
			return classElements;
		})(classname, node);
	}

}

T.gcl = function(node, classname) {
	return T.gcls(node, classname)[0];

}

T.gui = function(element, value) {
	return T.gat(element, 'data-ui', value);
}

T.gat = function(element, attribute, value) {
	return T.gats(element,attribute,value)[0];

}

T.gats = function(element, attribute, value) {
	var tags = element.getElementsByTagName('*');
	var gats = [];
	for (var i = 0; i < tags.length; i++) {
		if (tags[i].getAttribute(attribute) == value || (value=='*' && tags[i].hasAttribute(attribute))) {
			gats.push(tags[i]);
		}

	}
	return gats;
}

T.gid = function(id) {
	return document.getElementById(id);
}

T.adcl = function(element, classname) {
	var curClassnames = element.className.split(' ');
	if (curClassnames.indexOf(classname) > -1) return;
	curClassnames.push(classname);
	curClassnames = T.uniq_arr(curClassnames);
	element.className = curClassnames.join(' ');


}

T.rplcl = function(element, oldclassname, newclassname) {
	T.rmcl(element, oldclassname);
	T.adcl(element, newclassname);
}

T.rmcl = function(element, classname) {
	if (!element.className) return;
	var curClassnames = element.className.split(' ');
	var ind = curClassnames.indexOf(classname);
	if (ind > -1) {
		curClassnames.splice(ind, 1);
	}
	curClassnames = T.uniq_arr(curClassnames);
	element.className = curClassnames.join(' ');
}


T.dlg = function(object, method) {
	var shim = function() {
		return method.apply(object, arguments);
	}
	return shim;
}


T.rnd = function(min, max) {
	return Math.floor(Math.random() * (1 + max - min)) + min;
};

T.get=function(url,callback){
	T.ajx(url,null,"GET",callback)
}

T.post=function(url,data,callback){
    T.ajx(url,data,"POST",callback);
}

T.put=function(url,data,callback){
    T.ajx(url,data,"PUT",callback)
}


T.ajx = function(url, data, method, callback) {
	T.adcl(document.body,'loading');
    var oReq = createXHR();

	oReq.addEventListener("progress",
		function(evt) {
			//console.log(evt);
		}, false);

	oReq.addEventListener("load",
		function(evt) {
			//console.log(evt);
			var response = evt.currentTarget.responseText;
			var json;
			if (response) {
				try {
					json = JSON.parse(response)
				} catch (e) {
					console.error("Error with JSON response: " + response + ", complete error:" + e);
					console.error(e);
				}
			}
			callback(null,json);
            T.rmcl(document.body,'loading');

		}, false);
	oReq.addEventListener("error",
		function(evt) {
			console.log(evt);
		}, false);
	oReq.addEventListener("abort",
		function(evt) {
			console.log(evt);
		}, false);
	oReq.open(method, url, true);
	oReq.setRequestHeader('Cache-Control', 'no-cache');
    oReq.setRequestHeader('Accept', 'application/json');
	oReq.setRequestHeader('Content-Type', 'application/json');
	if(data){
        oReq.send(JSON.stringify(data));
    }else{
        oReq.send();
    }


	function createXHR(){
	    var xhr;
	    if (window.ActiveXObject){
	        try{
	            xhr = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        catch(e){
	            xhr = null;
	        }
	    }
	    else{
	        xhr = new XMLHttpRequest();
	    }
	    return xhr;
	}
}

T.timeString=function(seconds){
	var d = new Date(seconds*1000);
	var hoursLeft = Math.floor((seconds / 3600));
	var minutesLeft =d.getMinutes().toString();
	var secondsLeft = d.getSeconds().toString();

	if(minutesLeft.length==1)minutesLeft='0'+minutesLeft;
	if(secondsLeft.length==1)secondsLeft='0'+secondsLeft;


	return hoursLeft+":"+minutesLeft+":"+secondsLeft;
}
// taken from http://stackoverflow.com/questions/1068834/object-comparison-in-javascript
// The ORDER of the properties IS IMPORTANT, so this method will return false for following objects:
T.isEqualObject=function(object1,object2){
    var str1;
    var str2;
    try{
        str1 = JSON.stringify(object1);
        str2 = JSON.stringify(object2);
    }catch(e){
        console.error('isEqualObject error',e);
    }
    return str1 === str2;
}
//deep cloning of object
T.cloneObject=function(object){
    var clone;
    try{
        var string = JSON.stringify(object);
        clone = JSON.parse(string);
    }catch(e){
        console.error('cloneOject error',e)
    }
	return clone;
}

T.isObject=function(obj){
    return (typeof obj === typeof {});
}
