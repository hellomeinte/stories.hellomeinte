
function getQualityRequirement(){
    var w = document.documentElement.clientWidth
    if(w<=SD_WIDTH) return 'sd'
    if(w<=MD_WIDTH) return 'md'
    if(w<=HD_WIDTH) return 'hd'
    return 'xl'
}

function initImages(){
    var imgs = document.querySelectorAll('img');
    for(var i=0;i<imgs.length;i++){
        var img = imgs[i];
        if(img.className.indexOf('preloader')>-1)continue
        initImage(img)        

    }
}

function initImage(img){
    img.style.display='none'
    var div = document.createElement('div');
    div.className = img.className+' image_replacement';
    div.style.backgroundImage="url('"+img.src+"')"
    div.setAttribute('data-img-source',img.src)

    img.parentNode.insertBefore(div,img)
    T.del(img)

    return div;
}

function showFirstImage(){
    var section = document.getElementsByTagName('section')[0]
    var img = section.querySelectorAll('.dynamic_quality')[0]
    var div = initImage(img)


    div.style.backgroundImage="url('"+T.gid('first_image').src+"')"
    setTimeout(function(){
        T.rmcl(section,'loading')
    },500)
}