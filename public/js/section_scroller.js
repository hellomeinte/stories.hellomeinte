/* Author : M.A. van't kruis http://hellomeinte.com */
/* based on http://www.thepetedesign.com/demos/purejs_onepage_scroll_demo.html */
function section_scroller(_parent,_minWidth,_minHeight){
    var sections                =   null,
        parent                  =   _parent,
        previousSectionIndex    =   0,
        currentSectionIndex     =   0,
        minWidth                =   _minWidth,
        minHeight               =   _minHeight,
        touchStartY             =   0,
        isAnimating             =   false,
        mousewheelDisabled      =   false,
        beforeCallback          =   null,
        afterCallback           =   null

    function init(onBefore,onAfter){
        beforeCallback = onBefore
        afterCallback = onAfter
        sections = parent.getElementsByTagName('section')
        window.addEventListener('resize',resizeHandler)
        resizeHandler()
    }

    function mouseWheelHandler(e){
        e.preventDefault();
        if(mousewheelDisabled){
            return;
        }

        var delta = e.wheelDelta || -e.detail;
        var isMoving;
        if(delta < 0) isMoving= moveDown()
        else isMoving= moveUp()

        if(isMoving)mousewheelDisabled=true;
    }

    function keyHandler(e){
        switch(e.which) {
            case 38:
                moveUp()
                break;
            case 40:
                moveDown()
                break;
        }
    }

    function touchstartHandler(e) {
        var touches = e.touches;
        if (touches && touches.length) {
            touchStartY = touches[0].pageY;
            document.addEventListener("touchmove", touchMoveHandler);
            document.addEventListener("touchend", touchEndHandler);
        }
        e.preventDefault();
    }

    function touchEndHandler(e){
        document.removeEventListener('touchmove', touchMoveHandler);
        e.preventDefault();
    }

    function touchMoveHandler(e) {
        var touches = e.touches;
        if (touches && touches.length) {
            e.preventDefault();
            var deltaY = touchStartY - touches[0].pageY
            if (deltaY >= 50)moveDown()
            if (deltaY <= -50)moveUp()
  
            if (Math.abs(deltaY) >= 50) {
                document.removeEventListener('touchmove', touchMoveHandler)
            }
        }
        e.preventDefault();
    }
    //-webkit-transform: translate3d(0, " + pos + "%, 0);

    function moveUp(){
        previousSectionIndex = currentSectionIndex
        currentSectionIndex--
        if(currentSectionIndex<0)currentSectionIndex=0
        if(previousSectionIndex==currentSectionIndex) return false
        transform()
        return true
    }

    function moveDown(){
        previousSectionIndex = currentSectionIndex
        currentSectionIndex++
        if(currentSectionIndex>sections.length-1)currentSectionIndex = sections.length-1
        if(previousSectionIndex==currentSectionIndex) return false
        transform()
        return true
    }
    /*-----------------------------------------------------------*/
    /*  Transtionend Normalizer by Modernizr                     */
    /*-----------------------------------------------------------*/

    function whichTransitionEvent(){
        var t
        var el          = document.createElement('fakeelement')
        var transitions = {
            'transition':'transitionend',
            'OTransition':'oTransitionEnd',
            'MozTransition':'transitionend',
            'WebkitTransition':'webkitTransitionEnd'
        }

        for(t in transitions){
            if( el.style[t] !== undefined ){
                return transitions[t];
            }
        }
    }

    function getTransformStyle(position){
        var prefixes        =   ['-webkit-','-moz-','-ms-'],
            baseStyle       =   'transform: translate3d(0,'+position+'%,0);',
            style           =   baseStyle

        for(var i=0;i<prefixes.length;i++){
            style+=prefixes[i]+''+baseStyle;
        }

        return style;
    }

    function transform(){
        disable();
        if(isAnimating)return;

        if(beforeCallback)beforeCallback(previousSectionIndex,sections[previousSectionIndex])
        isAnimating=true;

        parent.addEventListener(whichTransitionEvent(), animationEndedHandler, false);
        parent.style.cssText = getTransformStyle(currentSectionIndex*-100);
    }

    function animationEndedHandler(e){
        enable();
        isAnimating=false;
        parent.removeEventListener(whichTransitionEvent(),animationEndedHandler)
        if(afterCallback)afterCallback(currentSectionIndex,sections[currentSectionIndex])
        setTimeout(function(){mousewheelDisabled=false},500)
    }

    function enable(delayMousewheel){
        document.addEventListener('mousewheel', mouseWheelHandler);
        document.addEventListener('keyup', keyHandler);
        document.addEventListener("touchstart", touchstartHandler);  

    }

    function disable(){
        document.removeEventListener('mousewheel', mouseWheelHandler);
        document.removeEventListener('keyup', keyHandler);
        document.removeEventListener("touchstart", touchstartHandler);
        document.removeEventListener("touchmove", touchMoveHandler);
        parent.removeEventListener(whichTransitionEvent(),animationEndedHandler)

    }

    function resizeHandler(e){
        var winWidth = document.documentElement.clientWidth;
        var winHeight = document.documentElement.clientHeight;
        if(false){//winWidth>minWidth && winHeight>minHeight){
            document.body.scrollTop=0
            enable()
        }else{
            disable()
            currentSectionIndex=0
            parent.style.cssText = getTransformStyle(currentSectionIndex*-100)
        }
    }

    return{
        init:init
    }
}