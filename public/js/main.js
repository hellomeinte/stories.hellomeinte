/* Author : M.A. van't kruis http://hellomeinte.com */
var bodyClass,
    headerDimensions,
    headerSVG,
    players=[]



function preinit(){
    headerDimensions = document.getElementById('headertext').getBBox(),
    headerSVG = document.getElementById('headersvg')
    bodyClass = document.body.className
    initImages()
    loadYoutubeApi()
    resizeHeader()
}

function loadYoutubeApi(){
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    window.onYouTubeIframeAPIReady=function(){
        console.log('youtube api ready');
    }
}
function init(){
    console.log('init')
    loadQualityImage(document.getElementsByTagName('section')[1]); //first image was already preloaded
    window.addEventListener('resize',window_resizeHandler)
    T.rmcl(document.body,'loading')
    window_resizeHandler()
    section_scroller(T.gid('main'),700,600).init(onBeforeMove,onAfterMove)
    
}

function onBeforeMove(index,elem){
    if(players[index]){
        console.log('pause video',index)
        players[index].pauseVideo();
    }
}
function onAfterMove(index,elem){
    if(!elem)return;
    loadQualityImage(elem)
    if(!players[index]){
        
        var youtubePlayer = elem.querySelectorAll('.youtube')[0];
        if(youtubePlayer && YT.Player){

            players[index] = new YT.Player(youtubePlayer, {
              events: {
                'onReady': function(e){
                    if(index==e.target.section){
                        console.log('onready play video',index)
                        e.target.playVideo()
                    }else{
                        console.log('onready pause video',index)
                        e.target.pauseVideo()
                    }
                    
                }
              }
            });
            players[index].section = index;
        }
    }else{
        console.log('play video',index)
        players[index].playVideo();
    }

    
}

function loadQualityImage(elem){
    var imgs=[];
    elem = getSection(elem)
    if(!elem)return imgs;

    var quality = getQualityRequirement();
    var elems = elem.querySelectorAll('.image_replacement');
    for(var i=0;i<elems.length;i++){
        var elem = elems[i]
        var currenSource = elem.getAttribute('data-img-source')
        
        if(currenSource.indexOf('/xs/')<0) continue; //already a replaced img
        var newSource = currenSource.replace('/xs/','/'+quality+'/')
        var loader = new Image()
        elem.setAttribute('data-img-source',newSource)
        loader.addEventListener('load',getLoadListener(elem))
        loader.src = newSource
        imgs.push(loader)
    }

    if(elems.length==0){
        loadNext(elem)
    }
    return imgs;
}

function getSection(elem){
    if(!elem || !elem.tagName)return null
    var tagName = elem.tagName.toLowerCase();
    if(tagName=='section') return elem
    else return getSection(elem.parentNode)
}

function getLoadListener(div){
    return function loaded(e){
        this.removeEventListener(loaded);
        div.style.backgroundImage="url('"+this.src+"')"
        T.rmcl(div.parentNode,'loading')
        //load next
        loadNext(div);

    }
}

function loadNext(elem){
    var nextSibling = getSection(elem).nextElementSibling
    loadQualityImage(nextSibling)
}

function resizeHeader () {
    headerDimensions = document.getElementById('headertext').getBBox()
    headersvg.style.width=headerDimensions.width+'px'
}
function window_resizeHandler(e){
    resizeHeader();
}
