var story={
    id:"fraser_flight",
    title:"Fraser Flight",
    sections:[
        {
            type:'type_0',
            images:[
                {
                    img:'p1130152.jpg',
                    title:'Cessna 172 on Orchid Beach, Fraser Island',
                    
                }
            ],
            story_blocks:[
                "Frasier Island is the biggest Sand Island off the coast of Queensland, Australia. My pilot's license was just a few weeks old, it was Sunday, and we decided to just go for it.",
                "We took off from Redcliffe on a flight that took us one and a half hour, flight time, we spent the night on the island and flew back the next morning, to arrive at my desk at 8.30, in Brisbane."
            ]
            
        },
        {
            type:'type_2',
            images:[
                {
                    img:'map.png',
                    title:'Fraser Island flight Route',
                    
                }
            ],
            story_blocks:[
                "The initial plan was to fly to Kilcoy, a picturesque grass airfield, just north of Lake Somerset. This was mainly to practise shortfield grass landings, which would be the exact scenario on Orchid Beach on Fraser Island.",
                "The weather blocked the way though, so we opted to fly to Gympie instead and have a short break there.",
                "After Gympie, we headed straight for Maryborough as a waypoint and turned east towards Orchid Beach, the name of the field at Fraser Island"
            ]
            
        },
        {
            type:'type_3',
            videos:[
                {
                    type:'youtube',
                    reference:'dB5myJu8vug',
                    title:'Taking off from yred',
                    
                }
            ],
            story_blocks:[
                'Taking off from Redcliffe in RAQ, a Cessna 172'
            ]
            
        },
        {
            type:'type_4',
            images:[
                {
                    img:'p1120961.jpg',
                    title:'Weather closing in just after departing Redcliffe.',
                    
                }
            ],
            story_blocks:[
                "Quite quickly after departing Recliffe it became obvious that the route towards Kilcoy was filled with cloud and rain. We could've waited, but I opted to take the coastal route, just west of the Sunshine Coast airport, which had clearer weather."
            ]
        },
        {
            type:'type_5',
            images:[
                {
                    img:'p1120984.jpg',
                    title:'View down the nose. Weather opening up, sunshine, blue skies and white fluffy clouds.',
                    
                },
                {
                    img:'p1120985.jpg',
                    title:'View down the right wing. Weather opening up, sunshine, blue skies and white fluffy clouds.',
                    position:'top'
                    
                }
            ],
            story_blocks:[
                "The weather cleared up and the way to Gympie was beautiful, lush green, usually referred to as the Sunshine Coast hinterland, it is a favourite spot for Brisbane locals to visit during the weekend."
            ]
        },
        {
            type:'type_4',
            images:[
                {
                    img:'p1130011.jpg',
                    title:'Short final, Gympie',
                    
                }
            ],
            story_blocks:[
                "This was a shot taken during short final, Gympie Airport. We decided to land here for a little break and for me to practise approach, landing and departure on an unfamiliar airport.",
                "It proofed to be good practise! Joining an unfamiliar circuit can be tricky for a 'fresh' pilot such as myself, visualising the path is hard when you've never been there before, and as luck would have it there was some traffic in the pattern."
            ]
            
        },
        {
            type:'type_1',
            images:[
                {
                    img:'p1130033.jpg',
                    title:'Rainbow down our right wing',
                    
                }
            ],
            story_blocks:[
                "Having departed Gympie, the weather was still a little bit patchy, though perfectly fine to fly in and with awesome rainbows around us!"
            ]
            
        },
         {
            type:'type_2',
            images:[
                {
                    img:'p1130040.jpg',
                    title:'Fraser Island flight Route',
                    
                }
            ],
            story_blocks:[
                "This is the Mary River, near Maryborough, it leads straight to the ocean.",
                "Very near to the coast, this is just before we cross the ocean for Fraser Island, very excited!",
                "This would also proof to be the last bit of cloud, as most patches did not cross the coast."
            ]
            
        },
        {
            type:'type_3',
            videos:[
                {
                    type:'youtube',
                    reference:'B5F0nL-1LgQ',
                    title:'Approaching Fraser Island',
                    
                }
            ],
            story_blocks:[
                'Approaching Fraser Island'
            ]
            
        },
        {
            type:'type_5',
            images:[
                {
                    img:'p1130069.jpg',
                    title:'Fraser Island',
                    
                },
                {
                    img:'p1130064.jpg',
                    title:'Meinte & Melissa'
                    
                }
            ],
            story_blocks:[
                "Crossing over to Fraser Island. The skies are blue again and the ocean looks stunning, with small sand islands popping up in blue waters before the Fraser itself starts."
            ]
        },
        {
            type:'type_2',
            images:[
                {
                    img:'p1130093.jpg',
                    title:'Fraser Island',
                    
                }
            ],
            story_blocks:[
                "The Orchid Beach landing field is just visible under the pitot tube(the pointy thing sticking out under the wing)",
                "The camping ground, which we walked from the airfield is farther down, near the bottom part of the beach, where the bushes start."
            ]
        },
        {
            type:'type_3',
            videos:[
                {
                    type:'youtube',
                    reference:'gtutJJUM1hA',
                    title:'Landing on Fraser Island',
                    
                }
            ],
            story_blocks:[
                'Landing on Orchid Beach, Fraser Island!'
            ]
            
        },
        {
            type:'type_1',
            images:[
                {
                    img:'p1130151.jpg',
                    title:'Cessna 172 on Orchid Beach, Fraser Island',
                    
                }
            ],
            story_blocks:[
                "Tied the plane down, paid landing costs in the local pub, and left for Waddy Point, hopefully the plane would still be there the next day"
            ]
            
        },
        {
            type:'type_6',
            images:[
                {
                    img:'p1130116.jpg',
                    title:'Fraser Island',
                    
                },
                {
                    img:'p1130118.jpg',
                    title:'Fraser Island',
                    
                },
                {
                    img:'p1130127.jpg',
                    title:'Fraser Island',
                    
                }
            ],
            story_blocks:[
                "It was quite a hike to the camping. The only way to get on Fraser is either by car(with a boat) or by plane, unless you take the boat and leg it. Most people assumed we walked the entire way there!"
            ]
        },
        {
            type:'type_4',
            images:[
                {
                    img:'p1130145.jpg',
                    title:'Starry night at Fraser Island',
                    
                }
            ],
            story_blocks:[
                "Our little blue tent under a starry night."
            ]
            
        },

    ]
}

function getStoryById(id,callback){
    
    callback(null,story)
}

function getRandomStory(callback){
    callback(null,story);
}

module.exports=function(db){
    return{
        getStoryById:getStoryById,
        getRandomStory:getRandomStory
    }
}