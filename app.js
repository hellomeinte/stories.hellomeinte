var express     = require('express'),
    path        = require('path'),
    models      = require('./models')()
    services    = require('./services')()
    routes      = require('./routes/index')(models,services),
    app         = express()

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

require('./middleware/pre_routing')(app)
app.use('/', routes);
require('./middleware/post_routing')(app)

module.exports = app