var debug       = require('debug')('hellomeinte:service:imageResize'),
    async       = require('async'),
    fs          = require('fs'),
    path        = require('path'),
    mkdirp      = require('mkdirp'),
    gm          = require('gm'),
    source_dir,
    cache_dir


function get_cachePath(imagePath,quality){
    return path.join(cache_dir,get_cacheName(imagePath,quality))
}

function get_originalPath(imagePath){
    return path.join(source_dir,imagePath)
}

function get_cacheName(imagePath,quality){
    return path.join(quality,imagePath)
}
function has_cache(imagePath,quality,callback){
    fs.exists(get_cachePath(imagePath,quality), function (exists) {
      callback(exists)
    });

}
function get_imageQuality(imagePath,quality,maxWidth,callback){
    has_cache(imagePath,quality,function(hasCache){
        if(hasCache){
            callback(null,get_cachePath(imagePath,quality))
        }else{
            var cachePath = get_cachePath(imagePath,quality).split('/');
            cachePath.pop();
            mkdirp(cachePath.join('/'), function(err) { 
                if(err){
                    callback(err);
                    return
                }
                var width;
                if(isNaN(quality))
                    width= process.env[quality.toUpperCase()+'_WIDTH']
                else
                    width = parseInt(quality)

                if(!isNaN(maxWidth) && width>maxWidth)width=maxWidth;
                resize(get_originalPath(imagePath),get_cachePath(imagePath,quality),width,callback)
            });
        }
    })
    
}

function resize(originalPath,outputPath,width,callback){
    outputPath = outputPath.replace('.png','.jpg')
    outputPath = outputPath.toLowerCase()
    gm(originalPath)
    .resize(width+'>')
    .autoOrient()
    .noProfile()
    .interlace('Line') //progressive jpeg
    .quality(90)
    .setFormat('jpg')
    .write(outputPath, function (err) {
        if (err)debug(err)
        callback(err,outputPath)
    });
}


module.exports=function(_source_dir,_cache_dir){
    source_dir = _source_dir
    cache_dir = _cache_dir
    return{
        get_imageQuality:get_imageQuality
    }
}