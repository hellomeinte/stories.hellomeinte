module.exports=function(){
    var sourceImagePath='.'+process.env.ORIGINAL_IMAGES_PATH,
        cacheImagePath = './public/img/cache' 
    return {
        imageQuality:require('./imageQuality')(sourceImagePath,cacheImagePath)
    }
}