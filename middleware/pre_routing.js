var bodyParser = require('body-parser'),
    path = require('path'),
    express = require('express')

module.exports=function(app){
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(express.static(path.join(__dirname, '../public')))
}